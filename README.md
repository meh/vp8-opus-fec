# Build

``` shell
meson build
ninja -C build
```

# Run

Receiver:

``` shell
./build/smpte-2022-1-qt-receiver
```

Sender:

``` shell
gst-launch-1.0  -v  rtpbin name=rtp fec-encoders='fec,0="rtpst2022-1-fecenc\ rows\=5\ columns\=5\ pt\=97";' \
  uridecodebin uri=file:///home/meh/Videos/the-great-gatsby-720p-trailer.mov name=d \
    d. ! video/x-raw ! queue ! vp8enc deadline=1 error-resilient=default threads=12 cpu-used=-16 keyframe-max-dist=30 target-bitrate=2560000 ! \
      queue ! rtpvp8pay ssrc=0 pt=96 ! rtp.send_rtp_sink_0
      rtp.send_rtp_src_0 ! udpsink host=127.0.0.1 port=5000 \
      rtp.send_fec_src_0_0 ! udpsink host=127.0.0.1 port=5002 async=false \
      rtp.send_fec_src_0_1 ! udpsink host=127.0.0.1 port=5004 async=false \
    d. ! audio/x-raw ! queue ! audioconvert ! opusenc inband-fec=true packet-loss-percentage=100 bitrate=128000 bitrate-type=vbr ! \
      queue ! rtpopuspay pt=98 ! rtp.send_rtp_sink_1 rtp.send_rtp_src_1 ! udpsink host=127.0.0.1 port=5006
```
