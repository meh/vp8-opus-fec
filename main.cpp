#include <QApplication>
#include <QTimer>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QtDebug>
#include <gst/gst.h>
#include <gst/video/videooverlay.h>
#include "pipeline.h"

int main(int argc, char *argv[])
{
  Pipeline *pipeline;
	gst_init(&argc, &argv);
	QApplication app(argc, argv);
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit ()));
	QWidget window;
	QWidget *widget = new QWidget();
	widget->setFixedSize(640,360);
	QVBoxLayout qboxLayout;
	qboxLayout.addWidget(widget);
	window.setLayout(&qboxLayout);
	window.resize(660, 380);
	window.show();

	WId wid = widget->winId();
  pipeline = new Pipeline();
  pipeline->Setup((gpointer) wid);
  pipeline->Play();

	app.exec();

  pipeline->Stop();
  delete pipeline;
}

