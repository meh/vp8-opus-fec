#ifndef PIPELINE_H
#define PIPELINE_H

#include <QObject>
#include <condition_variable>
#include <functional>
#include <gst/gst.h>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

class Pipeline : public QObject
{
  Q_OBJECT

public:
  explicit Pipeline(QObject* parent = 0);
  ~Pipeline();

public:
  enum class Status
  {
    INITIAL,
    READY,
    BUSY,
    STOPPED,
  };
  Q_ENUM(Status)

public:
  bool Setup(gpointer windowId);
  bool Play();
  void Stop();

  GstClockTime GetPosition();
  GstClockTime GetDuration();

  void ChangeStatus(Status status);

signals:
  void statusChanged(Status status);

private:
  static GstBusSyncReply busSyncHandler(GstBus* bus,
                                        GstMessage* msg,
                                        Pipeline* self);
  static gboolean busHandler(GstBus* bus, GstMessage* msg, Pipeline* self);
  static void mainLoopFunc(Pipeline* self);

  GstElement* mPipeline = nullptr;

  GMainContext* mMainContext = nullptr;
  GMainLoop* mMainLoop = nullptr;

  std::shared_ptr<std::thread> mMainThread;
  std::mutex mLock;

  Status mStatus = Status::INITIAL;
  gpointer mWindowId;

  void rtpBinPadAddedCb(GstElement *rtpbin, GstPad *pad);
  void decodeBinPadAddedCb(GstElement *decodebin, GstPad *pad);
  void decodeBinDeepElementAddedCb(GstElement *bin, GstElement *subbin, GstElement *element);
};

#endif // PIPELINE_H
