#include <QDebug>
#include <gst/video/video.h>
#include <iostream>

#include "pipeline.h"

Pipeline::Pipeline(QObject* parent)
  : QObject(parent)
{
  mMainContext = g_main_context_new();
  mMainLoop = g_main_loop_new(mMainContext, FALSE);
}

Pipeline::~Pipeline()
{
  qDebug() << "Pipeline destroyed";

  g_main_loop_quit(mMainLoop);
  if (mMainThread)
    mMainThread->join();
  g_main_loop_unref(mMainLoop);
  g_main_context_unref(mMainContext);
}

#define MAKE_AND_ADD(var, pipe, name, label)                                   \
  G_STMT_START                                                                 \
  {                                                                            \
    GError* make_and_add_err = NULL;                                           \
    if (G_UNLIKELY(!(var = (gst_element_factory_make(name, NULL))))) {         \
      GST_ERROR(                                                               \
        "Could not create element %s (%s)", name, make_and_add_err->message);  \
      g_clear_error(&make_and_add_err);                                        \
      goto label;                                                              \
    }                                                                          \
    if (G_UNLIKELY(!gst_bin_add(GST_BIN_CAST(pipe), var))) {                   \
      GST_ERROR("Could not add element %s", name);                             \
      goto label;                                                              \
    }                                                                          \
  }                                                                            \
  G_STMT_END

void
Pipeline::decodeBinPadAddedCb(GstElement *decodebin, GstPad *pad)
{
  GstCaps *caps;
  GstStructure *s;
  GstElement *sink = NULL;

  caps = gst_pad_get_current_caps(pad);
  s = gst_caps_get_structure(caps, 0);

  if (gst_structure_has_name(s, "video/x-raw"))
    MAKE_AND_ADD(sink, mPipeline, "autovideosink", err);
  else if (gst_structure_has_name(s, "audio/x-raw"))
    MAKE_AND_ADD(sink, mPipeline, "autoaudiosink", err);

  if (sink != NULL) {
    GstPad *sinkpad;
    GstPadLinkReturn link_ret;

    sinkpad = gst_element_get_static_pad(sink, "sink");
    link_ret = gst_pad_link (pad, sinkpad);
    gst_object_unref (sinkpad);

    if (link_ret != GST_PAD_LINK_OK) {
      GST_ERROR ("Failed to link sink");
      goto err;
    }

    gst_element_sync_state_with_parent(sink);
  }

err:
  return;
}

void
Pipeline::decodeBinDeepElementAddedCb(GstElement *bin, GstElement *subbin, GstElement *element)
{
  GstElementFactory *factory;

  factory = gst_element_get_factory(element);
  if (!g_strcmp0 (gst_plugin_feature_get_name(GST_PLUGIN_FEATURE(factory)), "opusdec")) {
    g_object_set (element, "use-inband-fec", TRUE, "plc", TRUE, NULL);
  }
}
void
Pipeline::rtpBinPadAddedCb(GstElement *rtpbin, GstPad *pad)
{
  GstElement *decodebin;
  GstPad *sinkpad;
  GstPadLinkReturn link_ret;

  MAKE_AND_ADD(decodebin, mPipeline, "decodebin", err);

  g_signal_connect(decodebin, "pad-added",G_CALLBACK(
    +[](GstElement *decodebin, GstPad *pad, Pipeline *self)
    {
      self->decodeBinPadAddedCb (decodebin, pad);
    }
  ), this);

  g_signal_connect(decodebin, "deep-element-added",G_CALLBACK(
    +[](GstElement *bin, GstElement *subbin, GstElement *element, Pipeline *self)
    {
      self->decodeBinDeepElementAddedCb (bin, subbin, element);
    }
  ), this);

  sinkpad = gst_element_get_static_pad(decodebin, "sink");
  link_ret = gst_pad_link (pad, sinkpad);
  gst_object_unref (sinkpad);

  if (link_ret != GST_PAD_LINK_OK) {
    GST_ERROR ("Failed to link decodebin");
    goto err;
  }

  gst_element_sync_state_with_parent(decodebin);

err:
  return;
}

bool
Pipeline::Setup(gpointer windowId)
{
  bool ret = true;
  mPipeline = gst_pipeline_new(NULL);
  GstElement *rtpbin, *column_fec_src, *row_fec_src, *vp8_src, *opus_src;
  GstCaps *caps;
  GstStructure *s;
  GstBus* bus;

  if (mStatus not_eq Status::INITIAL) {
    qWarning() << "Pipeline was already set up";
    goto err;
  }

  MAKE_AND_ADD(rtpbin, mPipeline, "rtpbin", err);
  MAKE_AND_ADD(column_fec_src, mPipeline, "udpsrc", err);
  MAKE_AND_ADD(row_fec_src, mPipeline, "udpsrc", err);
  MAKE_AND_ADD(vp8_src, mPipeline, "udpsrc", err);
  MAKE_AND_ADD(opus_src, mPipeline, "udpsrc", err);

  s = gst_structure_new("fec-decoders", "0", G_TYPE_STRING, "rtpst2022-1-fecdec", NULL);
  g_object_set (rtpbin, "latency", 500, "fec-decoders", s, NULL);
  gst_structure_free(s);

  if (!gst_element_link_pads(column_fec_src, "src", rtpbin, "recv_fec_sink_0_0")) {
    GST_ERROR ("Failed to link column FEC src");
    goto err;
  }

  if (!gst_element_link_pads(row_fec_src, "src", rtpbin, "recv_fec_sink_0_1")) {
    GST_ERROR ("Failed to link row FEC src");
    goto err;
  }

  if (!gst_element_link_pads(vp8_src, "src", rtpbin, "recv_rtp_sink_0")) {
    GST_ERROR ("Failed to link VP8 src");
    goto err;
  }

  if (!gst_element_link_pads(opus_src, "src", rtpbin, "recv_rtp_sink_1")) {
    GST_ERROR ("Failed to link OPUS src");
    goto err;
  }

  g_signal_connect(rtpbin, "pad-added",G_CALLBACK(
    +[](GstElement *rtpbin, GstPad *pad, Pipeline *self)
    {
      self->rtpBinPadAddedCb (rtpbin, pad);
    }
  ), this);

  caps = gst_caps_new_simple("application/x-rtp", "payload", G_TYPE_INT, 96,
      "clock-rate", G_TYPE_INT, 90000,
      "encoding-name", G_TYPE_STRING, "VP8",
      "media", G_TYPE_STRING, "video",
      NULL);
  g_object_set(vp8_src, "caps", caps, "address", "127.0.0.1", "port", 5000, NULL);
  gst_caps_unref (caps);

  caps = gst_caps_new_simple("application/x-rtp", "payload", G_TYPE_INT, 98,
      "clock-rate", G_TYPE_INT, 48000,
      "encoding-name", G_TYPE_STRING, "OPUS",
      "media", G_TYPE_STRING, "audio",
      NULL);
  g_object_set(opus_src, "caps", caps, "address", "127.0.0.1", "port", 5006, NULL);
  gst_caps_unref (caps);

  caps = gst_caps_new_simple("application/x-rtp", "payload", G_TYPE_INT, 97, NULL);
  g_object_set(column_fec_src, "caps", caps, "address", "127.0.0.1", "port", 5002, NULL);
  g_object_set(row_fec_src, "caps", caps, "address", "127.0.0.1", "port", 5004, NULL);
  gst_caps_unref (caps);

  gst_pipeline_set_auto_flush_bus(GST_PIPELINE(mPipeline), FALSE);

  mWindowId = windowId;

  bus = gst_element_get_bus(mPipeline);
  gst_bus_set_sync_handler(
    bus, (GstBusSyncHandler)Pipeline::busSyncHandler, this, nullptr);
  gst_object_unref(bus);

  ChangeStatus(Status::READY);

done:
  return ret;

err:
  ret = false;
  goto done;
}

bool
Pipeline::Play()
{
  std::lock_guard<std::mutex> Lock(mLock);

  if (not mPipeline) {
    qDebug() << "No pipeline to play";
    return false;
  }

  if (mStatus not_eq Status::READY) {
    qDebug() << "Busy processing another command";
    return false;
  }

  // Now play, it is OK to set the state upward even if the
  // previous call returned ASYNC
  switch (gst_element_set_state(mPipeline, GST_STATE_PLAYING)) {
    case GST_STATE_CHANGE_ASYNC:
      qDebug() << "Changing state async";
      ChangeStatus(Status::BUSY);
      break;
    case GST_STATE_CHANGE_FAILURE:
      qCritical() << "Failure changing pipeline state";
      return false;
    default:
      break;
  }

  mMainThread = std::make_shared<std::thread>(Pipeline::mainLoopFunc, this);

  return true;
}

void
Pipeline::Stop()
{
  std::lock_guard<std::mutex> Lock(mLock);

  if (not mPipeline) {
    qDebug() << "No pipeline to stop";
    return;
  }

  g_main_loop_quit(mMainLoop);
}

GstClockTime
Pipeline::GetPosition()
{
  std::lock_guard<std::mutex> Lock(mLock);
  gint64 pos = -1;

  if (not mPipeline)
    return GST_CLOCK_TIME_NONE;

  if (gst_element_query_position(mPipeline, GST_FORMAT_TIME, &pos)) {
    return (GstClockTime)pos;
  } else {
    qDebug() << "Couldn't query position";

    return GST_CLOCK_TIME_NONE;
  }
}

GstClockTime
Pipeline::GetDuration()
{
  std::lock_guard<std::mutex> Lock(mLock);
  gint64 dur = -1;

  if (not mPipeline)
    return GST_CLOCK_TIME_NONE;

  if (gst_element_query_duration(mPipeline, GST_FORMAT_TIME, &dur)) {
    return (GstClockTime)dur;
  } else {
    qDebug() << "Couldn't query duration";

    return GST_CLOCK_TIME_NONE;
  }
}

static void
printErrorOrWarningMsg(GstMessage* msg, bool isError)
{
  GError* err = NULL;
  gchar *name, *debug = NULL;
  QString type = "Error";

  name = gst_object_get_path_string(msg->src);
  if (isError) {
    gst_message_parse_error(msg, &err, &debug);
  } else {
    gst_message_parse_warning(msg, &err, &debug);
    type = "Warning";
  }

  qDebug() << type << " from " << name << ": " << err->message;
  if (debug)
    qDebug() << "Additional debug info: " << debug;

  g_clear_error(&err);
  g_free(debug);
  g_free(name);
}

GstBusSyncReply
Pipeline::busSyncHandler(GstBus* bus, GstMessage* msg, Pipeline* self)
{
  Q_UNUSED(bus);

  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_ERROR:
      GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(self->mPipeline),
                                        GST_DEBUG_GRAPH_SHOW_ALL,
                                        "error");

      printErrorOrWarningMsg(msg, true);
      g_main_loop_quit(self->mMainLoop);
      break;
    case GST_MESSAGE_STATE_CHANGED: {
      GstState old, new_, pending;

      if (GST_MESSAGE_SRC(msg) != GST_OBJECT_CAST(self->mPipeline))
        break;

      gst_message_parse_state_changed(msg, &old, &new_, &pending);

      qDebug() << "State changed: " << gst_element_state_get_name(new_);

      GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(self->mPipeline),
                                        GST_DEBUG_GRAPH_SHOW_ALL,
                                        gst_element_state_get_name(new_));

      if (new_ == GST_STATE_NULL) {
        qDebug() << "Stopped";
        self->ChangeStatus(Status::STOPPED);
      }

      break;
    }
    default:
      break;
  }

  if (gst_is_video_overlay_prepare_window_handle_message (msg)) {
    gst_video_overlay_set_window_handle (GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (msg)), (guintptr) self->mWindowId);
  }

  return GST_BUS_PASS;
}

void
Pipeline::ChangeStatus(Pipeline::Status status)
{
  // Don't advertise that we're ready on ASYNC_DONE after set_state(NULL)
  if (mStatus == Status::STOPPED)
    return;

  qDebug() << "Changing status";
  mStatus = status;
  emit statusChanged(status);
}

gboolean
Pipeline::busHandler(GstBus* bus, GstMessage* msg, Pipeline* self)
{
  Q_UNUSED(bus);

  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_CLOCK_LOST:
      qDebug() << "Clock Lost";
      gst_element_set_state(self->mPipeline, GST_STATE_PAUSED);
      gst_element_set_state(self->mPipeline, GST_STATE_PLAYING);
      break;
    case GST_MESSAGE_EOS:
      qDebug() << "Got EOS";
      g_main_loop_quit(self->mMainLoop);
      break;
    case GST_MESSAGE_WARNING:
      printErrorOrWarningMsg(msg, false);
      break;
    case GST_MESSAGE_REQUEST_STATE: {
      GstState state;
      gchar* name;

      name = gst_object_get_path_string(GST_MESSAGE_SRC(msg));

      gst_message_parse_request_state(msg, &state);

      qDebug() << "Setting state to " << gst_element_state_get_name(state)
               << " as requested by " << name;

      gst_element_set_state(self->mPipeline, state);
      g_free(name);
      break;
    }
    case GST_MESSAGE_ASYNC_DONE: {
      self->ChangeStatus(Status::READY);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

void
Pipeline::mainLoopFunc(Pipeline* self)
{
  g_main_context_push_thread_default(self->mMainContext);

  GstBus* bus = gst_element_get_bus(self->mPipeline);
  gst_bus_add_watch(bus, (GstBusFunc)Pipeline::busHandler, self);
  gst_object_unref(bus);

  /* We want to know when we reach NULL */
  gst_pipeline_set_auto_flush_bus(GST_PIPELINE(self->mPipeline), FALSE);

  qDebug() << "Starting main loop";
  g_main_loop_run(self->mMainLoop);
  qDebug() << "Stopped main loop";

  g_main_context_pop_thread_default(self->mMainContext);

  // Clean up
  std::lock_guard<std::mutex> Lock(self->mLock);
  if (self->mPipeline) {
    GstBus* bus = gst_element_get_bus(self->mPipeline);

    if (gst_element_set_state(self->mPipeline, GST_STATE_NULL) ==
        GST_STATE_CHANGE_FAILURE)
      self->ChangeStatus(Status::READY);

    gst_bus_set_flushing(bus, TRUE);
    gst_bus_remove_watch(bus);
    gst_object_unref(bus);
    gst_clear_object(&self->mPipeline);
  }

  qDebug() << "Exit main thread";
}
